import { combineReducers } from 'redux';

const addCustomerReducer = (oldListCustomers=[], action) => {
    if (action.type === 'ADD_CUSTOMER') {
        return [...oldListCustomers, action.customer]
    }
    return oldListCustomers
}

const getTotalsReducer = (oldTotalsData={}, action) => {
    if (action.type === 'GET_TOTAL') {
        return {...action.totals}
    }
    return oldTotalsData
}

export default combineReducers({
    customers: addCustomerReducer,
    totals: getTotalsReducer
})