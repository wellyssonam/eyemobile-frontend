// Action creator
export const addCustomer = (data) => {
    // Return an action
    return {
        type: 'ADD_CUSTOMER',
        customer: data
    }
}

export const getTotals = (data) => {
    // Return an action
    return {
        type: 'GET_TOTAL',
        totals: data
    }
}