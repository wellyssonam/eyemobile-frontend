import React, { Component } from 'react';
import './Table.scss';

class Table extends Component {

    render() {
        return (
            <div className="table">
            { this.props.items.length > 0 ? (
                <table>
                    <thead>
                        <tr>
                            <th className="title">ID</th>
                            <th className="title">NOME</th>
                            <th className="title">DOCUMENTO</th>
                            <th className="title">DATA NASCIMENTO</th>
                            <th className="title">CLIENTE DESDE</th>
                            <th className="title">ÚLTIMA COMPRA</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.props.items.map((item, index) => {
                                return (
                                    <tr key={index}>
                                        <td className="text">{ item.id }</td>
                                        <td className="text">{ item.name }</td>
                                        <td className="text">{ item.document }</td>
                                        <td className="text">{ item.birthdate }</td>
                                        <td className="text">{ item.customer_since }</td>
                                        <td className="text">{ item.last_purchase }</td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table> ) : (
                    <div className="warning">
                        CLIENTE NÃO ENCONTRADO!!!
                    </div>
                )
            }
            </div>
        );
    }
}

export default Table

Table.defaultProps = {
    items: []
}