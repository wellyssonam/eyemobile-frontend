import React, { Component } from 'react';
// import Chart from 'chart.js'
import { Chart, Bar, Line, Pie } from 'react-chartjs-2';

class Graph extends Component {
    constructor(props) {
        super(props)
        this.state = {
            chartGraph: {
                data: {},
                options: {}
            }
        }
        // this.canvasRef = React.createRef()
    }

    componentDidMount() {
        const canvas_ctx = this.refs.canvas
        canvas_ctx.lineJoin = 'round'
        canvas_ctx.lineWidth = 20
        canvas_ctx.width = 300
        canvas_ctx.height = 300
        // canvas_ctx.canvas.clientWidth = 300
        // canvas_ctx.save()

        Chart.defaults.global.elements.point.radius = 20;

        var myChart = new Chart(canvas_ctx, {
            type: 'bar',
            data: {
                labels: ['Red', 'Blue'],
                datasets: [
                    {
                        label: 'Receitas',
                        data: [1100,2200],
                        backgroundColor: [
                            'rgba(255, 99, 132)',
                            'rgba(54, 162, 235)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132)',
                            'rgba(54, 162, 235)'
                        ],
                        borderWidth: 1
                    }
                ]
            },
            options: {
                responsive: true,
                title: {
                    display: this.props.displayTitle,
                    text: 'Largest',
                    fontSize: 25
                },
                legend: {
                    display: this.props.displayLegend,
                    position: this.props.legendPosition,
                    labels: {
                        fontColor: '#000'
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
        canvas_ctx['lineJoin'] = 'round'
        canvas_ctx.lineWidth = 20
    }

    getChartData = (canvas) => {
        var ctx = document.getElementById('myChart');
        const data = this.state.chartGraph.data
        const options = this.state.chartGraph.options
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: data,
            options: options
        })
    }

    render() {
        return (
            <div className="graph">
                <canvas ref="canvas" id="myChart"></canvas>
                {/* <Bar
                    ref={ this.canvasRef }
                    data={ this.getChartData }
                    options={ this.state.chartGraph.options }
                /> */}
            </div>
        );
    }
}

export default Graph;

Graph.defaultProps = {
    displayTitle: true,
    displayLegend: true,
    legendPosition: 'bottom'
}