import React, { Component } from 'react';
// import Chart from 'chart.js'
import { Chart, Bar, Line, Pie } from 'react-chartjs-2';

class Graph extends Component {
    constructor(props) {
        super(props)
        this.state = {
            chartGraph: {
                data: {},
                options: {}
            }
        }
        this.canvasRef = React.createRef()
    }

    componentDidMount() {
        const canvas = this.canvasRef.current
        
        this.setState({
            chartGraph: {
                data: {
                    labels: ['Red', 'Blue'],
                    datasets: [
                        {
                            label: 'Receitas',
                            data: [1100,2200],
                            backgroundColor: [
                                'rgba(255, 99, 132)',
                                'rgba(54, 162, 235)'
                            ],
                            borderColor: [
                                'rgba(255, 99, 132)',
                                'rgba(54, 162, 235)'
                            ],
                            borderWidth: 1
                        }
                    ]
                },
                options: {
                    responsive: true,
                    title: {
                        display: this.props.displayTitle,
                        text: 'Largest',
                        fontSize: 25
                    },
                    legend: {
                        display: this.props.displayLegend,
                        position: this.props.legendPosition,
                        labels: {
                            fontColor: '#000'
                        }
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            }
        })
    }

    getChartData = (canvas) => {
        const data = this.state.chartGraph.data
        const ctx = canvas.getContext('2d')
        ctx.lineJoin = 'round'
        ctx.lineWidth = 20
        return data
    }

    render() {
        return (
            <div className="graph">
                <Bar
                    ref={ this.canvasRef }
                    data={ this.getChartData }
                    options={ this.state.chartGraph.options }
                />
            </div>
        );
    }
}

export default Graph;

Graph.defaultProps = {
    displayTitle: true,
    displayLegend: true,
    legendPosition: 'bottom'
}