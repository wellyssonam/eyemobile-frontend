import React, { Component } from 'react';
import { Bar, Doughnut } from 'react-chartjs-2';

class Graph extends Component {
    constructor(props) {
        super(props)
        this.canvasRef = React.createRef()
    }

    getGraphData = () => {
        var bgColors = this.props.graph.bgColors
        var myDatasets
        var myData
        if (this.props.typeGraphBar) {
            myDatasets = this.props.datas.map(obj => {
                let currentKey = Object.keys(obj)[0]
                let currentValue = Object.values(obj)[0]
                return {
                    label: currentKey,
                    data: [currentValue],
                    backgroundColor: [ bgColors[currentKey] ],
                    borderWidth: 0
                }
            });
            myData = { datasets: myDatasets }
        } else {
            let listLabels = Object.keys(this.props.graph.bgColors);
            let listDataValues = this.props.datas.map(obj => {
                return Object.values(obj)[0] 
            });
            let listBgColors = this.props.datas.map(obj => bgColors[Object.keys(obj)[0]] );
            myDatasets = [{
                // cria-se um vetor data, com os valores a ser dispostos no gráfico
                data: listDataValues,
                // cria-se uma propriedade para adicionar cores aos respectivos valores do vetor data
                backgroundColor: listBgColors
            }]
            myData = {
                datasets: myDatasets,
                labels: listLabels
            }
        }

        return myData
    }

    getChartData = (canvas) => {
        const data = this.getGraphData()
        const ctx = canvas.getContext('2d')
        ctx.lineJoin = 'round'
        ctx.lineWidth = 20
        return data
    }

    render() {
        let chartOptions = {
            options: {
                responsive: true,
                title: {
                    display: this.props.displayTitle,
                    text: this.props.graph.title,
                    fontSize: 25,
                    fontFamily: 'fontDinCondensed',
                    fontColor: '#737689'
                },
                legend: {
                    display: this.props.displayLegend,
                    position: this.props.legendPosition,
                    labels: {
                        fontColor: '#4a4a4a',
                        fontSize: 12,
                        fontFamily: 'fontAvenir'
                    }
                },
                legendCallback: function(chart) {
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: false
                        }
                    }]
                }
            }
        }

        return (
            <div className={this.props.typeGraphBar ? 'graph-expenses' : 'graph-services'}>
                {
                    this.props.typeGraphBar ?
                        (
                            <Bar
                                ref={ this.canvasRef }
                                data={ this.getChartData }
                                options={ chartOptions }
                            />
                        ) : (
                            <Doughnut
                                ref={ this.canvasRef }
                                data={ this.getChartData }
                                options={ this.getChartOptions }
                            />
                        )
                }
            </div>
        );
    }
}

export default Graph;

Graph.defaultProps = {
    displayTitle: true,
    displayLegend: true,
    legendPosition: 'bottom',
    typeGraphBar: true,
    graph: {
        title: ''
    }
}