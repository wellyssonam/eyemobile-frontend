import React, { Component } from 'react';
import ReactSVG from 'react-svg';
import './Tabs.scss';

class Tabs extends Component {
    constructor(props) {
        super(props)
        this.state = {
            tabOptions: [
                {
                    name: 'TOTAIS',
                    icon: require('../../images/ico/ic_totais.svg')
                },
                {
                    name: 'CLIENTES',
                    icon: require('../../images/ico/ic_clientes.svg')
                }
            ]
        }
    }

    render() {
        return (
            <div className="tabs-em">
                <ul className="tab">
                    { this.state.tabOptions.map((tab, index) => {
                        return (
                            <li
                                key={ index }
                                className={`tablinks ${this.props.tabChoice === index ? 'active' : ''}`}
                                onClick={ () => this.props.updateTabChoice(index) }
                            >
                                <ReactSVG src={ tab.icon } />
                                <div className="tablinks-name"><span>{ tab.name }</span></div>
                            </li>
                        );
                    })}
                </ul>
            </div>
        )
    }
}

Tabs.defaultProps = {
    tabChoice: 0
}

export default Tabs;