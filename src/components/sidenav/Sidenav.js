import React, { Component } from 'react';
import './Sidenav.scss';

class Sidenav extends Component {
    render() {
        let logo = require('../../images/ico/ic_logo.svg')

        return (
            <div className="sidenav sidenav-fixed hide-mobile">
                <span
                    className="closebtn hide-on-med-and-up"
                    onClick={ this.props.closeNav }
                >×</span>
                <ul>
                    <li className="item-logo"><img className="logo" src={ logo } alt="" /></li>
                    { this.props.children }
                </ul>
            </div>
        )
    }
}

export default Sidenav