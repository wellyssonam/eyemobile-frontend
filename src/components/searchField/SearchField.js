import React, { Component } from 'react';
import './SearchField.scss';

class SearchField extends Component {
    onFormSubmit = (event) => {
        event.preventDefault()
    }
    render() {
        return (
            <div className="search-field" onSubmit={ this.onFormSubmit }>
                <form className="search-field-form">
                    <button type="submit"><i className="material-icons search-icon">search</i></button>
                    <input
                        type="text"
                        className="search-field"
                        placeholder="PESQUISAR NOME"
                        name="search"
                        onChange={ (e) => this.props.onChangeSearchField(e) }
                    />
                </form>
            </div>
        );
    }
}

export default SearchField