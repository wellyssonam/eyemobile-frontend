import React, { Component } from 'react';
import Sidenav from '../sidenav/Sidenav';
import Tabs from '../navpath/Navpath';
import Home from '../pages/home/Home';
import { Link } from 'react-router-dom';
import { Switch, Route } from 'react-router-dom';
import ReactSVG from 'react-svg';
import './App.scss';
import ErrorPage404 from '../pages/error/ErrorPage404';
import $ from 'jquery';

class App extends Component {

    closeMenu = () => {
        // Actions to close sidebarmenu with jquery
        var sidebar = $('.sidenav')
        sidebar.removeClass('open')
        $('.bg-page').removeClass('open')
        setTimeout(() => {
            sidebar.addClass('hide-mobile')
        }, 1000);
    }

    render() {
        let dashboard_icon = require('../../images/ico/ic_dashboard.svg')
        let cadastre_icon = require('../../images/ico/ic_cadastro.svg')

        return (
            <div className="app">
                <div
                    className="bg-page close"
                    onClick={ this.closeMenu }
                ></div>
                <header>
                    <Sidenav closeNav={ this.closeMenu }>
                        <Link className="link" to="/">
                            <li className="item-menu" onClick={ this.closeMenu }>
                                <div>
                                    <ReactSVG src={ dashboard_icon } />
                                    <div className="item-name">Meu Faturamento</div>
                                </div>
                            </li>
                        </Link>
                        <Link className="link" to="/" onClick={ this.closeMenu }>
                            <li className="item-menu cadastre disabled">
                                <div>
                                    <ReactSVG src={ cadastre_icon } />
                                    <div className="item-name">Cadastro</div>
                                </div>
                            </li>
                        </Link>
                    </Sidenav>
                    <Tabs />
                </header>
                <main>
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col m12">
                                <Switch>
                                    <Route path="/" exact={true} component={Home} />
                                    <Route path="*" exact={true} component={ErrorPage404} />
                                </Switch>
                            </div>
                        </div>
                    </div>
                </main>
                <footer></footer>
            </div>
        );
    }
}

export default App;