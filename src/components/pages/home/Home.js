import React, { Component } from 'react';
import './Home.scss';
import Tabs from '../../tabs/Tabs';
import Totals from '../totals/Totals';
import Clients from '../clients/Clients';
import { connect } from 'react-redux';
import { addCustomer, getTotals } from '../../../actions';
import Loader from 'react-loader-spinner';

class Home extends Component {

    state = {
        spinner: false,
        componentReady: false,
        tabChoice: 0,
        graphs: {
            expenses: {
                title: 'DESPESAS X RECEITAS',
                bgColors: {
                    'revenue': '#01DCAA',
                    'expenses': '#FE4F64'
                }
            },
            services: {
                title: 'SERVIÇOS',
                bgColors: {
                    "Banho & Tosa": '#CC29D5',
                    "Consultas": '#7929D4',
                    "Medicamentos": '#3A86FE'
                }
            }
        }
    }

    spinner = false

    componentDidMount = () => {
        // context: Guardará as informações que seriam buscadas na API
        this.spinner = true
        var context = {
            totals: {
                "total": 12840.41,
                "services" : [
                    { "Banho & Tosa": 6445.20 },
                    { "Consultas": 3867.15 },
                    { "Medicamentos": 5239.96 }
                ],
                "revenue": 15552.31,
                "expenses": 2711.90,
                "transactions": [
                    {
                        "id": 4512,
                        "amount": 644.52,
                        "type": "Receitas",
                        "product_id": 26,
                        "product_name": "Banho & Tosa",
                        "time": "2019-07-26 10:00:40.000000"
                    },
                    {
                        "id": 4513,
                        "amount": 644.52,
                        "type": "Receitas",
                        "product_id": 26,
                        "product_name": "Banho & Tosa",
                        "time": "2019-07-26 10:10:40.000000"
                    },
                    {
                        "id": 4514,
                        "amount": 644.52,
                        "type": "Receitas",
                        "product_id": 26,
                        "product_name": "Banho & Tosa",
                        "time": "2019-07-26 10:12:40.000000"
                    },
                    {
                        "id": 4515,
                        "amount": 644.52,
                        "type": "Receitas",
                        "product_id": 26,
                        "product_name": "Banho & Tosa",
                        "time": "2019-07-27 10:12:40.000000"
                    },
                    {
                        "id": 4516,
                        "amount": 644.52,
                        "type": "Receitas",
                        "product_id": 26,
                        "product_name": "Banho & Tosa",
                        "time": "2019-08-27 10:12:40.000000"
                    },
                    {
                        "id": 4517,
                        "amount": 644.52,
                        "type": "Receitas",
                        "product_id": 26,
                        "product_name": "Banho & Tosa",
                        "time": "2019-08-27 10:12:40.000000"
                    },
                    {
                        "id": 4518,
                        "amount": 644.52,
                        "type": "Receitas",
                        "product_id": 26,
                        "product_name": "Banho & Tosa",
                        "time": "2019-04-10 14:12:40.000000"
                    },
                    {
                        "id": 4519,
                        "amount": 644.52,
                        "type": "Receitas",
                        "product_id": 26,
                        "product_name": "Banho & Tosa",
                        "time": "2019-04-10 15:12:40.000000"
                    },
                    {
                        "id": 4520,
                        "amount": 644.52,
                        "type": "Receitas",
                        "product_id": 26,
                        "product_name": "Banho & Tosa",
                        "time": "2019-04-10 15:16:40.000000"
                    },
                    {
                        "id": 4521,
                        "amount": 644.52,
                        "type": "Receitas",
                        "product_id": 26,
                        "product_name": "Banho & Tosa",
                        "time": "2019-04-10 15:28:40.000000"
                    },
                    {
                        "id": 5512,
                        "amount": 773.43,
                        "type": "Receitas",
                        "product_id": 27,
                        "product_name": "Consultas",
                        "time": "2019-06-10 10:00:40.000000"
                    },
                    {
                        "id": 5513,
                        "amount": 773.43,
                        "type": "Receitas",
                        "product_id": 27,
                        "product_name": "Consultas",
                        "time": "2019-07-11 10:10:40.000000"
                    },
                    {
                        "id": 5514,
                        "amount": 773.43,
                        "type": "Receitas",
                        "product_id": 27,
                        "product_name": "Consultas",
                        "time": "2019-08-16 10:12:40.000000"
                    },
                    {
                        "id": 5515,
                        "amount": 773.43,
                        "type": "Receitas",
                        "product_id": 27,
                        "product_name": "Consultas",
                        "time": "2019-08-17 10:12:40.000000"
                    },
                    {
                        "id": 5516,
                        "amount": 773.43,
                        "type": "Receitas",
                        "product_id": 27,
                        "product_name": "Consultas",
                        "time": "2019-08-17 10:50:40.000000"
                    },
                    {
                        "id": 5512,
                        "amount": 515.62,
                        "product_id": 28,
                        "type": "Receitas",
                        "product_name": "Medicamentos",
                        "time": "2019-06-10 10:00:40.000000"
                    },
                    {
                        "id": 5513,
                        "amount": 515.62,
                        "product_id": 28,
                        "type": "Receitas",
                        "product_name": "Medicamentos",
                        "time": "2019-07-11 10:10:40.000000"
                    },
                    {
                        "id": 5514,
                        "amount": 515.62,
                        "product_id": 28,
                        "type": "Receitas",
                        "product_name": "Medicamentos",
                        "time": "2019-08-16 10:12:40.000000"
                    },
                    {
                        "id": 5515,
                        "amount": 515.62,
                        "product_id": 28,
                        "type": "Receitas",
                        "product_name": "Medicamentos",
                        "time": "2019-08-17 10:12:40.000000"
                    },
                    {
                        "id": 5516,
                        "amount": 515.62,
                        "product_id": 28,
                        "type": "Receitas",
                        "product_name": "Medicamentos",
                        "time": "2019-08-17 10:50:40.000000"
                    },
                    {
                        "id": 5517,
                        "amount": 715.62,
                        "product_id": 28,
                        "type": "Receitas",
                        "product_name": "Medicamentos",
                        "time": "2019-10-5 10:50:40.000000"
                    },
                    {
                        "id": 5518,
                        "amount": 920.62,
                        "product_id": 28,
                        "type": "Receitas",
                        "product_name": "Medicamentos",
                        "time": "2019-9-25 10:50:40.000000"
                    },{
                        "id": 5519,
                        "amount": 1025.62,
                        "product_id": 28,
                        "type": "Receitas",
                        "product_name": "Medicamentos",
                        "time": "2019-10-6 10:50:40.000000"
                    },
                    {
                        "id": 6512,
                        "amount": 2711.90,
                        "product_id": 29,
                        "type": "Despesas",
                        "product_name": "Folha de pagamento",
                        "time": "2019-06-10 10:00:40.000000"
                    }
                ]
            },
            customers: [
                {
                    "id": 54878,
                    "name": "Lorem Lipsun",
                    "document": "05017892001",
                    "birthdate": "26/07/1993",
                    "customer_since": "30/09/2019",
                    "last_purchase": "30/09/2019"
                },
                {
                    "id": 54879,
                    "name": "Lorem Lipsun",
                    "document": "21380512000",
                    "birthdate": "18/06/1995",
                    "customer_since": "30/09/2019",
                    "last_purchase": "30/09/2019"
                },
                {
                    "id": 54880,
                    "name": "Lorem Lipsun",
                    "document": "96295342078",
                    "birthdate": "12/05/1990",
                    "customer_since": "30/09/2019",
                    "last_purchase": "30/09/2019"
                },
                {
                    "id": 54881,
                    "name": "Lorem Lipsun",
                    "document": "07449635089",
                    "birthdate": "06/01/1986",
                    "customer_since": "30/09/2019",
                    "last_purchase": "30/09/2019"
                },
                {
                    "id": 54882,
                    "name": "Lorem Lipsun",
                    "document": "22169125060",
                    "birthdate": "30/09/1995",
                    "customer_since": "30/09/2019",
                    "last_purchase": "30/09/2019"
                },
                {
                    "id": 54883,
                    "name": "Lorem Lipsun",
                    "document": "04353161091",
                    "birthdate": "03/08/1984",
                    "customer_since": "30/09/2019",
                    "last_purchase": "30/09/2019"
                },
                {
                    "id": 54884,
                    "name": "Lorem Lipsun",
                    "document": "81096395002",
                    "birthdate": "27/08/1996",
                    "customer_since": "30/09/2019",
                    "last_purchase": "30/09/2019"
                },
                {
                    "id": 54885,
                    "name": "Lorem Lipsun",
                    "document": "92840743043",
                    "birthdate": "28/11/1990",
                    "customer_since": "30/09/2019",
                    "last_purchase": "30/09/2019"
                },
                {
                    "id": 54886,
                    "name": "Lorem Lipsun",
                    "document": "41811506070",
                    "birthdate": "08/06/1967",
                    "customer_since": "30/09/2019",
                    "last_purchase": "30/09/2019"
                },
                {
                    "id": 54887,
                    "name": "Lorem Lipsun",
                    "document": "78376198033",
                    "birthdate": "12/11/1965",
                    "customer_since": "30/09/2019",
                    "last_purchase": "30/09/2019"
                },
                {
                    "id": 54888,
                    "name": "Francisco de Paula",
                    "document": "78376198033",
                    "birthdate": "12/11/1965",
                    "customer_since": "30/09/2019",
                    "last_purchase": "30/09/2019"
                }
            ]
        }

        // Update redux with totals values
        this.props.getTotals(context.totals)

        // Update redux with customers values
        context.customers.forEach(data => {
            this.props.addCustomer(data)
        });

        this.setState({
            componentReady: true
        })
        this.spinner = false
    }

    changeTabOption = (index) => {
        this.setState({
            tabChoice: index
        })
    }

    render() {
        return (
            <div className="home">
            { this.spinner &&
                <Loader
                    className="spinner"
                    type="Oval"
                    color="#09CFA3"
                    height={100}
                    width={100}
                    timeout={0} //3 secs

                />

            }
                <Tabs
                    tabChoice={ this.state.tabChoice }
                    updateTabChoice={ this.changeTabOption }
                />
                { this.state.componentReady &&
                    <div className="content">
                    { this.state.tabChoice === 0 ?
                        (
                            <Totals graphs={ this.state.graphs } />
                        ) : (
                            <Clients />
                        )
                    }
                    </div>
                }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return { customers: state.customers, totals: state.totals }
}

export default connect(
    mapStateToProps,
    { addCustomer, getTotals }
)(Home);