import React, { Component } from 'react';
import SearchField from '../../searchField/SearchField';
import Table from '../../table/Table';
import { connect } from 'react-redux';

class Clients extends Component {

    state = {
        customersAfterSearch: []
    }

    onChangeSearchFieldReceived = (event) => {
        event.preventDefault()
        this.spinner = true
        let text = event.target.value
        let filterCustomers = this.props.customers.filter(customer => {
            return customer.name.toLowerCase().indexOf(text.toLowerCase()) > -1;
        })
        this.setState({
            customersAfterSearch: filterCustomers
        })
    }
    
    render() {
        return (
            <div className="clients">
                <div className="col s12">
                    <SearchField onChangeSearchField={ this.onChangeSearchFieldReceived }/>
                </div>
                <div className="col s12">
                    <Table items={ this.state.customersAfterSearch.length > 0 ? this.state.customersAfterSearch : this.props.customers } />
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return { customers: state.customers, totals: state.totals }
}

export default connect(mapStateToProps)(Clients);