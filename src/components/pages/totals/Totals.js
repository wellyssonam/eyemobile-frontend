import React, { Component } from 'react';
import './Totals.scss';
import Filter from '../../filter/Filter';
import Graph from '../../graph/Graph';
import { connect } from 'react-redux';

class Totals extends Component {

    state = {
        isReady: false,
        totalsLocal: {
            total: '0.00',
            services: [{ "Banho & Tosa": 0.00 }, { "Consultas": 0.00 }, { "Medicamentos": 0.00 }],
            revenue: '0.00',
            expenses: '0.00'
        }
    }

    componentDidMount = () => {
        this.getDatasByFilter()
        
    }

    convertValueBr = (value) => {
        //Instanciando o objeto
        var formatter = new Intl.NumberFormat('pt-BR', {
            style: 'currency',
            currency: 'BRL',
            minimumFractionDigits: 2,
        });

        /* FORMATA CONFORME CONFIGURAÇÕES DEFINIDAS NO formatter */
        let newValue = formatter.format(value); 
        return newValue
    }

    getDatasByFilter = (filterOption) => {
        let alias = filterOption ? filterOption : 'default'
        let resFilter = this.props.totals.transactions.filter(item => {
            if(alias === 'default') {
                return item
            }
            
            let today = new Date()
            let dateItem = new Date(item.time)
            let dateCompare = new Date()
            let dateStart, dateEnd
            
            if(alias === 'lastMonth') {
                dateCompare.setMonth(dateCompare.getMonth() - 1)
                dateStart = new Date(dateCompare.getFullYear(), dateCompare.getMonth(), 1)
                dateEnd = new Date()
                dateEnd.setDate(1)
                dateEnd.setDate(dateEnd.getDate() - 1)
                return dateStart <= dateItem && dateItem <= dateEnd
            } else if(alias === 'lastWeek') {
                dateCompare.setDate(dateCompare.getDate() - 7)
                return dateCompare <= dateItem && dateItem < today
            } else {
                return new Date(item.time).toDateString() === new Date().toDateString()
            }
        })

        var total = 0.00
        var revenue = 0.00
        var expenses = 0.00
        var services = { "Banho & Tosa": 0.00, "Consultas": 0.00, "Medicamentos": 0.00 }
        var servicesList = []

        resFilter.forEach(res => {
            if(res.type === 'Despesas') {
                expenses += res.amount
            } else {
                revenue += res.amount
            }
            services[res.product_name] += res.amount
        });

        servicesList = [
            { "Banho & Tosa": services["Banho & Tosa"].toFixed(2) },
            { "Consultas": services["Consultas"].toFixed(2) },
            { "Medicamentos": services["Medicamentos"].toFixed(2) }
        ]
        total = revenue - expenses

        this.setState({
            isReady: true,
            alias: alias,
            totalsLocal: {
                total: total.toFixed(2),
                revenue: revenue.toFixed(2),
                expenses: expenses.toFixed(2),
                services: servicesList
            }
        });
    }

    render() {
        return (
            <div className="totals">
                <Filter setDatasByFilter={ this.getDatasByFilter } />
                <div className="amount">
                    <div className="title">VALOR TOTAL</div>
                    <div className="value">{ this.convertValueBr(this.state.totalsLocal.total) }</div>
                    <hr className="line" />
                </div>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col s12 m12 l6">
                            <Graph
                                typeGraphBar={ false }
                                datas={ this.state.totalsLocal.services }
                                graph={ this.props.graphs.services }
                            />
                        </div>
                        <div className="col s12 m12 l6">
                            <Graph
                                datas={[
                                    { revenue: this.state.totalsLocal.revenue },
                                    { expenses: this.state.totalsLocal.expenses }
                                ]}
                                graph={ this.props.graphs.expenses }
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return { totals: state.totals }
}

export default connect(mapStateToProps)(Totals);