import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import './ErrorPage404.scss'

class ErrorPage404 extends Component {
    render() {
        return (
            <div className="error-page-404">
                <h1 className="error-number">404</h1>
                <p className="error-message">Página não encontrada</p>
            </div>
        );
    }
}

export default withTranslation('common')(ErrorPage404);