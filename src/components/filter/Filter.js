import React, { Component } from 'react';
import './Filter.scss';

class Filter extends Component {
    state = {
        currentKeyBtn: 'btnOther',
        buttons: []
    }

    componentDidMount = () => {
        this.setState({
            buttons: {
                btnToday: {
                    name: 'Hoje',
                    alias: 'today',
                    active: false
                },
                btnLastWeek: {
                    name: 'Última Semana',
                    alias: 'lastWeek',
                    active: false
                },
                btnLastMonth: {
                    name: 'Último Mês',
                    alias: 'lastMonth',
                    active: false
                },
                btnOther: {
                    name: 'Outro Período',
                    alias: 'default',
                    active: true
                }
            }
        })
    }

    changeFilterOption = (oldKeyBtn, newKeyBtn) => {
        let newObjButtons = this.state.buttons
        newObjButtons[oldKeyBtn].active = false
        newObjButtons[newKeyBtn].active = true
        this.setState({
            currentKeyBtn: newKeyBtn,
            buttons: newObjButtons
        })
        this.props.setDatasByFilter(newObjButtons[newKeyBtn].alias)
    }
    
    render() {
        let buttonsList = this.state.buttons
        return (
            <div className="filter">
                { Object.keys(buttonsList).map((btnKey, index) => {
                    return (
                        <button
                            className={`btn-link ${ buttonsList[btnKey].active ? 'active' : ''}`}
                            type="button"
                            key={ index }
                            onClick={ () => this.changeFilterOption(this.state.currentKeyBtn, btnKey) }
                        >
                            <span className="btn-link-name">{ buttonsList[btnKey].name.toUpperCase() }</span>
                        </button>
                    )
                })}
            </div>
        );
    }
}

export default Filter