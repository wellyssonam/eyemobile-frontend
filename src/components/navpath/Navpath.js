import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ReactSVG from 'react-svg';
import './Navpath.scss';
import $ from 'jquery';

class Navpath extends Component {

    openMenu = () => {
        $('.sidenav').removeClass('hide-mobile').addClass('open')
        $('.bg-page').addClass('open')
    }

    render() {
        let current_page = 'MEU FATURAMENTO'
        let iconMenu = require('../../images/ico/ic_menu.svg')

        return (
            <div className="nav-path">
                <ul className="nav-path-ul">
                    <li className="btn-menu hide-on-med-and-up" onClick={ this.openMenu }>
                        <ReactSVG src={ iconMenu } />
                    </li>
                    <Link className="link" to="/"><li className="nav-path-li">PETSHOP</li></Link>
                    <Link className="link" to="/"><li className="nav-path-li">{ current_page }</li></Link>
                </ul>
            </div>
        );
    }
}

export default Navpath