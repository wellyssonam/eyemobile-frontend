# Desafio | Frontend Developer

## Etapas de Instalação e Execução

1 -  Primeiro certifique-se de que possui o node instalado para utilizar os comandos npm, para isso em seu terminal digite:
> npm -v

2 - Se o node não estiver instalado acesse o site para download (**[node](https://nodejs.org/en/download/)**)

3 - Após instalação o projeto já poderá ser executado, para isso localize e acesse o diretório "span-linx" pelo terminal, verifique se encontra-se na branch "wellysson-melo" e então execute o comando para a instalação de todas as dependência presentes no projeto.
> npm install

4 - No mesmo diretório digite o camando para executar o projeto.
> npm start

---
## Sobre o Projeto

Para este desafio, criamos a ideia de um software para gestão de Petshops. Você precisará criar uma interface com duas abas, uma para apresentação de resultados, e outra para listagem de clientes.

---
### Tecnologias Utilizadas

- HTML5, CSS3, JavaScript
- Materialize
- React JS + Redux

---
### Não Foi Possível Fazer

- Não foi possível deixar o gráfico tipo "Bar" com os cantos arredondados.

---
### Melhorias Feitas

- Páginas responsivas para diferentes tamanhos de tela.
- O campo de busca é acionado ao digitar algum caractere.
- Gráficos apresentam efeitos em sua exibição.
- Uma página foi criada para exibir "Erro 404".

---
### Ajustes/Melhorias Futuras

- Não foi possível deixar o gráfico tipo "Bar" com os cantos arredondados.
- Em Desktop o software funciona com perfeição nos navegadores "chrome" e "firefox", porém apresenta problemas no "Safari" ao atualizar as informações ao mudar de filtro na tela "Totais" .

---
### Página Publicada

Acessar página **[eyemobile-frontend](https://eyemobile-frontend.wellyssonam.now.sh/)**