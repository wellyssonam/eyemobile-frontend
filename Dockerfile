FROM mhart/alpine-node:8.11.4
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package*.json .
RUN npm install
COPY . .
EXPOSE 3000
CMD [ "npm", "start" ]

# FROM node:lts-slim
# RUN mkdir -p /usr/src/app
# WORKDIR /usr/src/app
# COPY package*.json .
# RUN npm install --quiet
# RUN npm install react-scripts --silent
# COPY . .
# EXPOSE 3000
# CMD [ "npm", "start" ]
# ADD src /usr/src/app/src
# ADD public /usr/src/app/public
# RUN npm build
# EXPOSE 3000
# CMD [ "npm", "start" ]